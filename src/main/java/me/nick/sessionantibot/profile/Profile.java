package me.nick.sessionantibot.profile;

import lombok.Getter;
import lombok.Setter;
import me.nick.sessionantibot.SessionAntiBot;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class Profile {
    @Getter private static Map<String, Profile> profiles = new HashMap<>();

    @Getter @Setter private String ip;
    @Getter @Setter private String name;
    @Getter @Setter private int timeLoggedOn = 0;

    public Profile(String ip) {
        this.ip = ip;
    }

    public static void start() {
        SessionAntiBot.getInstance().getProxy().getScheduler().schedule(SessionAntiBot.getInstance(), new Runnable() {
            @Override
            public void run() {
                for (Profile p : Profile.getProfiles().values()) {
                    p.setTimeLoggedOn(p.getTimeLoggedOn() + 1);
                    if (p.getTimeLoggedOn() == 5) {
                        SessionAntiBot.getInstance().getIps().add(p.getIp());
                    }
                }
            }
        }, 1L, 1L, TimeUnit.MINUTES);
    }

}
