package me.nick.sessionantibot.profile;

import me.nick.sessionantibot.SessionAntiBot;
import net.md_5.bungee.api.event.PlayerDisconnectEvent;
import net.md_5.bungee.api.event.PostLoginEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

public class ProfileListener implements Listener {

    private SessionAntiBot instance = SessionAntiBot.getInstance();

    @EventHandler
    public void onLogin(PostLoginEvent e) {
        String ip = e.getPlayer().getAddress().toString();
        String ipNoSlash = ip.replace("/", "");
        String ipFinal = ipNoSlash.split(":")[0];
        if (instance.getIps().isEmpty() || !instance.getIps().contains(ipFinal)) {
            Profile profile = new Profile(ipFinal);
            profile.setName(e.getPlayer().getName());
            Profile.getProfiles().put(ipFinal, profile);
        }
    }

    @EventHandler
    public void onLeave(PlayerDisconnectEvent e) {
        String ip = e.getPlayer().getAddress().toString();
        String ipNoSlash = ip.replace("/", "");
        String ipFinal = ipNoSlash.split(":")[0];
        if (Profile.getProfiles().containsKey(ipFinal)) {
            Profile.getProfiles().remove(ipFinal);
        }
    }

}
