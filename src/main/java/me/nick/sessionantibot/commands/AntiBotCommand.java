package me.nick.sessionantibot.commands;

import me.nick.sessionantibot.SessionAntiBot;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class AntiBotCommand extends Command {

    private SessionAntiBot plugin = SessionAntiBot.getInstance();
    public AntiBotCommand() {
        super("antibot");
    }

    @Override
    public void execute(CommandSender commandSender, String[] args) {
        if (commandSender instanceof ProxiedPlayer) {
            commandSender.sendMessage(new TextComponent("Only console"));
            return;
        }

        if (args.length == 1) {
            if (args[0].equals("toggle")) {
                plugin.setAntiBotMode(!plugin.isAntiBotMode());
                plugin.setAntiBotModeActiveSeconds(0);
                commandSender.sendMessage(new TextComponent("AntiBotMode: " + plugin.isAntiBotMode()));
            }
        }
    }
}
