package me.nick.sessionantibot.commands;

import me.nick.sessionantibot.SessionAntiBot;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.plugin.Command;

public class CPSCommand extends Command {

    private SessionAntiBot plugin = SessionAntiBot.getInstance();
    public CPSCommand() {
        super("cps");
    }

    @Override
    public void execute(CommandSender commandSender, String[] args) {
        if (args.length == 0) {
            commandSender.sendMessage(new TextComponent("Connections per 2 seconds: " + Integer.toString(plugin.getJoinsPerSec())));
            commandSender.sendMessage(new TextComponent("AntiBotMode: " + plugin.isAntiBotMode()));
            if (plugin.isAntiBotMode()) commandSender.sendMessage(new TextComponent("AntiBotMode Toggled Since: " + plugin.getAntiBotModeActiveSeconds()));
        }
    }
}
