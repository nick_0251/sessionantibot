package me.nick.sessionantibot;

import lombok.Getter;
import lombok.Setter;
import me.nick.sessionantibot.bot.Bot;
import me.nick.sessionantibot.commands.AntiBotCommand;
import me.nick.sessionantibot.commands.CPSCommand;
import me.nick.sessionantibot.bot.BotListener;
import me.nick.sessionantibot.profile.Profile;
import me.nick.sessionantibot.profile.ProfileListener;
import net.md_5.bungee.api.plugin.Plugin;
import net.md_5.bungee.config.Configuration;
import net.md_5.bungee.config.ConfigurationProvider;
import net.md_5.bungee.config.YamlConfiguration;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.List;

public class SessionAntiBot extends Plugin {

    @Getter @Setter private static SessionAntiBot instance;
    @Getter @Setter public Configuration configuration;
    @Getter @Setter List<String> ips;
    @Getter @Setter private int joinsPerSec = 0;
    @Getter @Setter private boolean antiBotMode = false;
    @Getter @Setter private int antiBotModeActiveSeconds = 0;

    @Override
    public void onEnable() {
        //Register instance on start
        setInstance(this);

        //Register commands
        Arrays.asList(
                new CPSCommand(),
                new AntiBotCommand()
        ).forEach(command -> getProxy().getPluginManager().registerCommand(this, command));

        //Register listeners
        Arrays.asList(
                new BotListener(),
                new ProfileListener()
        ).forEach(listener -> getProxy().getPluginManager().registerListener(this, listener));

        loadConfig();

        //Start timers
        Bot.start();
        Profile.start();
    }

    public void onDisable() {
        SessionAntiBot.getInstance().configuration.set("savedIps", ips);
        SessionAntiBot.getInstance().saveConfig();
    }

    private void loadConfig() {
        if (!getDataFolder().exists())
            getDataFolder().mkdir();

        File file = new File(getDataFolder(), "config.yml");


        if (!file.exists()) {
            try (InputStream in = getResourceAsStream("config.yml")) {
                Files.copy(in, file.toPath());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        try {
            this.configuration = ConfigurationProvider.getProvider(YamlConfiguration.class).load(new File(getDataFolder(), "config.yml"));
            this.ips = this.getConfiguration().getStringList("savedIps");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void saveConfig() {
        try {
            ConfigurationProvider.getProvider(YamlConfiguration.class).save(configuration, new File(getDataFolder(), "config.yml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
