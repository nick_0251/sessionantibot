package me.nick.sessionantibot.bot;

import me.nick.sessionantibot.SessionAntiBot;
import java.util.concurrent.TimeUnit;

public class Bot {

    public static void start() {
        SessionAntiBot.getInstance().getProxy().getScheduler().schedule(SessionAntiBot.getInstance(), new Runnable() {
            SessionAntiBot instance = SessionAntiBot.getInstance();
            @Override
            public void run() {
                instance.setJoinsPerSec(0);
                if (instance.isAntiBotMode()) {
                    instance.setAntiBotModeActiveSeconds(instance.getAntiBotModeActiveSeconds() + 2);
                }
                if (instance.isAntiBotMode() && instance.getAntiBotModeActiveSeconds() > 300) {
                    instance.setAntiBotMode(false);
                    instance.setAntiBotModeActiveSeconds(0);
                }
            }
        }, 2L, 2L, TimeUnit.SECONDS);
    }

}
