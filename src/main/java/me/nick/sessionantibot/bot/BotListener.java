package me.nick.sessionantibot.bot;

import me.nick.sessionantibot.SessionAntiBot;
import net.md_5.bungee.api.event.PreLoginEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

public class BotListener implements Listener {

    private SessionAntiBot instance = SessionAntiBot.getInstance();

    @EventHandler
    public void onLogin(PreLoginEvent e) {
        String ip = e.getConnection().getAddress().toString();
        String ipNoSlash = ip.replace("/", "");
        String ipFinal = ipNoSlash.split(":")[0];
        if (instance.isAntiBotMode()) {
            if (!instance.getIps().contains(ipFinal)) {
                e.setCancelReason("You can't join right now, wait around 5 minutes before logging back in.");
                e.setCancelled(true);
            }
        } else {
            instance.setJoinsPerSec(instance.getJoinsPerSec() + 1);
            if (instance.getJoinsPerSec() > 20) {
                if (!instance.isAntiBotMode()) {
                    instance.setAntiBotMode(true);
                }
                if (!instance.getIps().contains(ipFinal)) {
                    e.setCancelReason("You can't join right now, wait around 5 minutes before logging back in.");
                    e.setCancelled(true);
                }
            }
        }
    }

}